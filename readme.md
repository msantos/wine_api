### Desafio DataOPs

---
#### Descrição:
Disponibilizar os dados do dataset Wine Reviews através de uma API para consulta.

#### Proposta de Infraestrutura Ideal:
Para tornar o sistema tolerante a falhas e resilinte, foi pensada em uma infraestrutura
implementada em Docker Swarm com o seguinte arranjo:

- Ambientes:
    - Ambiente Produtivo
    - Ambiente Produtivo Disaster Recovery
    - Modo de operação Ativo-Ativo
    
Observação:
Esta infraestrutura não está levando em consideração a camada de redes, ou seja, a implementação
e/ou configuração de Firewalls, DNS Server, Switches e outros.
    
### Descrição da infraestrutura de cada ambiente:

- API REST:
    - Python 3.8
    - Aplicação escrita em Django 3.0
    - Django Rest Framework para a implementação da camada API Rest 

- Load Balance:
    - Nginx
    - Instância para balanceamento de cargas HTTP ( API )
    - Instância para balanceamento de cargas TCP ( Cluster Postgres )
    
- Banco de Dados:
    - Postgres
    - Cluster com 3 Nós
    - Cluster Control Orquestrador
    
- Filas:
    - RabbitMQ 
    - Fila para enfileiramento das requisições
    - Fila para enfileiramento das respostas às requisições
    
#### Infraestrutura Mínima (MVP):
Para esta entrega foi implementada a seguinte infraestrutura:

- Ambientes:
    - Ambiente DEV

- API REST:
    - Python 3.8
    - Aplicação escrita em Django 3.0
    - Django Rest Framework para a implementação da camada API Rest 

- Proxy Reverso:
    - Nginx
    - Instância para Proxy das requisições HTTP
    
- Banco de Dados:
    - Postgres Standalone
    
#### Executar o projeto
Para rodar este projeto, você irá precisar ter o docker e o docker-compose instalados
no seu computador. Caso ainda não possua nenhum destes softwares, por favor, clique nos
links abaixo e realize a instalação:

Instalar Docker:

    https://docs.docker.com/engine/install/

Instalar Docker-Compose

    https://docs.docker.com/compose/install/

#### Execução do Projeto:

Realizar o clone do projeto deste repositório

Entrar dentro do diretório principal, onde se localiza o arquivo docker-compose.yml e 
executar o seguinte comando:

    docker-compose up -d
    
Após a execução deste comando o docker-compose irá realizar o download das imagens do projeto
e em seguida irá colocar a aplicação no ar. Assim que o sistema iniciar, uma das instancias irá
realizar o input de dados do dataset Wine. Esse processo vai levar alguns minutos mas você poderá
acessar a aplicação normalmente. Para acessar a aplicação acesse a seguinte URL em qualquer navegador:

    http://0.0.0.0/api/wine
    
Acessando esta URL você verá os 10 primeiros registros do banco de dados em uma lista
resumida, ou seja, somente com os campos id, country, designation e winery.

Para verificar as informações completas sobre um registro, escolha um registro da lista e
adicione o número ao final da URL da seguinte forma:

    http://0.0.0.0/api/wine/1
    
Este endpoint irá realizar uma busca no banco de dados e trazer todos os dados referentes ao 
registro com id 1.

#### Endpoints da API

##### Ações Básicas

Listagem de registros:

    http://0.0.0.0/api/wine
    
Detalhes de um registro:

    http://0.0.0.0/api/wine/<ID-REGISTRO>

##### Paginação
Para paginar os resultados das consultas utilize os parametros limit e offset.

Listar os 20 primeiros registros:

    http://0.0.0.0/api/wine?limit=20 

Listar os 20 próximos registros após os 20 primeiros:

    http://0.0.0.0/api/wine?limit=20&offset=20

Observação: 

- Por padrão, caso não seja passado nenhum dos parâmetros Offset e Limit a aplicação
configura os valores `limit=10` e `offset=0`, ou seja, os 10 primeiros registros no banco de dados

- O limite para retorno de registros em uma única requisição é de 100 registros, ou seja, caso o 
parâmetro limit seja maior que 100, ele só irá retornar os 100 primeiros registros que coincidam com 
a busca realizada.

##### Ordenação

Para ordernar os resultados por um campo específico utilize o parâmetro `ordering`:

    http://0.0.0.0/api/wine?ordering=country

Os campos disponíveis para ordenação são:
- country 
- variety
- points 
- price 
- winery

##### Filtragem

Para filtrar os resultados por um ou mais campos específicos utilize a seguinte sintaxe:

    http://0.0.0.0/api/wine?country=US&winery=Mirassou
    
Os campos disponíveis para filtrage são:
- country
- variety
- points 
- price 
- winery

##### Busca

A busca foi restrita ao campo `description`. Para realizar a busca de uma palavra chave
ou parte de uma palavra, utilize a seguint sintaxe:

    http://0.0.0.0/api/wine?description=Wood
    
##### Misturando tudo

É possível realizar uma busca ativando todos as funcionalidades de busca.
Para tal, é necessário apenas inserir os parâmetros de acordo com as suas regras de utilização:

    http://0.0.0.0/api/wine?description=Wood&country=US&ordering=winery&limit=10&offset=100
FROM python:3.7.6-alpine3.10

WORKDIR /app
COPY . .

RUN apk update && apk upgrade && \
    apk add libcurl zlib-dev libjpeg-turbo-dev libwebp tk openjpeg \
    curl-dev gcc g++ postgresql-dev gcc musl-dev && \
    python -m pip install -U --force-reinstall pip && \
    pip install psycopg2-binary

RUN pip install -r requirements.txt

EXPOSE 8000/tcp

CMD python manage.py runserver 0.0.0.0:8000 --noreload
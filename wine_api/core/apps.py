from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'wine_api.core'

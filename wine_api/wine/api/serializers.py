from rest_framework import serializers

from wine_api.wine.models import Wine


class WineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wine
        fields = ['id', 'country', 'designation', 'winery']


class WineDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wine
        fields = "__all__"

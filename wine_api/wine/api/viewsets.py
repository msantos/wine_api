from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, filters

from wine_api.wine.api.filters import CustomSearchFilter
from wine_api.wine.api.paginators import CustomPaginator
from wine_api.wine.api.serializers import WineSerializer, WineDetailSerializer
from wine_api.wine.models import Wine


class WineViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Wine.objects.all()
    filter_backends = [DjangoFilterBackend, CustomSearchFilter, filters.OrderingFilter]
    pagination_class = CustomPaginator
    search_fields = ['description']
    ordering_fields = filterset_fields = ['country', 'points', 'price', 'variety', 'winery']

    default_serializer_class = WineSerializer
    serializer_classes = {
        'list': WineSerializer,
        'retrieve': WineDetailSerializer,
    }

    @method_decorator(cache_page(60 * 60 * 2))
    def dispatch(self, *args, **kwargs):
        return super(WineViewSet, self).dispatch(*args, **kwargs)

    def get_serializer_class(self):
        return self.serializer_classes.get(self.action, self.default_serializer_class)

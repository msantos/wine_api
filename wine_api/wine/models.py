from django.db import models

from wine_api.core.models import BaseModel


class Wine(BaseModel):
    country = models.CharField(max_length=80, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    designation = models.CharField(max_length=150, null=True, blank=True)
    points = models.PositiveSmallIntegerField(null=True, blank=True)
    price = models.FloatField(null=True, blank=True)
    province = models.CharField(max_length=150, null=True, blank=True)
    region_1 = models.CharField(max_length=150, null=True, blank=True)
    region_2 = models.CharField(max_length=150, null=True, blank=True)
    taster_name = models.CharField(max_length=150, null=True, blank=True)
    taster_twitter_handle = models.CharField(max_length=80, null=True, blank=True)
    title = models.CharField(max_length=250, null=True, blank=True)
    variety = models.CharField(max_length=150, null=True, blank=True)
    winery = models.CharField(max_length=150, null=True, blank=True)

    def __str__(self):
        return self.designation

    @staticmethod
    def input(register):
        w = Wine()
        w.country = register[1]
        w.description = register[2]
        w.designation = register[3]
        w.points = register[4]
        w.price = None if register[5] == '' else register[5]
        w.province = register[6]
        w.region_1 = register[7]
        w.region_2 = register[8]
        w.taster_name = register[9]
        w.taster_twitter_handle = register[10]
        w.title = register[11]
        w.variety = register[12]
        w.winery = register[13]
        return w

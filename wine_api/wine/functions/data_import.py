import csv
import logging
import threading
from collections import defaultdict

from django.apps import apps

from wine_api.wine.models import Wine

logging.basicConfig(level=logging.INFO)


class BulkCreateManager:
    def __init__(self, chunk_size=100):
        self._create_queues = defaultdict(list)
        self.chunk_size = chunk_size

    def _commit(self, model_class):
        model_key = model_class._meta.label
        model_class.objects.bulk_create(self._create_queues[model_key])
        self._create_queues[model_key] = []

    def add(self, obj):
        model_class = type(obj)
        model_key = model_class._meta.label
        self._create_queues[model_key].append(obj)
        if len(self._create_queues[model_key]) >= self.chunk_size:
            self._commit(model_class)

    def done(self):
        for model_name, objs in self._create_queues.items():
            if len(objs) > 0:
                self._commit(apps.get_model(model_name))


class LogEntry(threading.Thread):
    def run(self):
        with open("wine_api/winemag.csv", "r") as winedoc:
            reader = csv.reader(winedoc)
            next(reader)
            bulk_mgr = BulkCreateManager(50)
            for row in reader:
                logging.info(row)
                bulk_mgr.add(Wine.input(row))
            bulk_mgr.done()

from os import environ

from django.apps import AppConfig


class WineConfig(AppConfig):
    name = 'wine_api.wine'

    def ready(self):
        if environ.get('INPUT_DATA') == "True":
            from wine_api.wine.functions.data_import import LogEntry
            log_entry = LogEntry()
            log_entry.daemon = True
            log_entry.start()
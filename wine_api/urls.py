from django.urls import path, include
from rest_framework import routers

from wine_api.wine.api import viewsets

router = routers.DefaultRouter()
router.register('api/wine', viewsets.WineViewSet)

urlpatterns = [
    path('', include(router.urls)),
]

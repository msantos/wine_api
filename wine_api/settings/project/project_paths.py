import os

import environ

root = environ.Path(__file__) - 3  # get root of the project
SITE_ROOT = root()

env = environ.Env()
environ.Env.read_env()

STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
public_root = root.path('public/')
MEDIA_ROOT = public_root('mediafiles')
MEDIA_URL = env.str('MEDIA_URL', default='media/')
STATIC_ROOT = public_root('staticfiles')
STATIC_URL = env.str('STATIC_URL', default='static/')

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(SITE_ROOT, '../../../templates')]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

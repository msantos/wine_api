import environ

env = environ.Env(DEBUG=(bool, False))
environ.Env.read_env()

DATABASES = {'default': env.db('DATABASE_URL')}

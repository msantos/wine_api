# Internationalization

LANGUAGE_CODE = 'pt-br'
TIME_ZONE = 'UTC'
USE_TZ = True
USE_I18N = True
USE_L10N = True

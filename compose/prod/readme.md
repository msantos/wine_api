### Desafio DataOPs

---
#### Descrição:
Disponibilizar os dados do dataset Wine Reviews através de uma API para consulta.

#### Proposta de Infraestrutura Ideal:
Para tornar o sistema tolerante a falhas e resilinte, foi pensada em uma infraestrutura
implementada em Docker Swarm com o seguinte arranjo:

- Ambientes:
    - Ambiente Produtivo
    - Ambiente Produtivo Disaster Recovery
    - Modo de operação Ativo-Ativo
    
Observação:
Esta infraestrutura não está levando em consideração a camada de redes, ou seja, a implementação
e/ou configuração de Firewalls, DNS Server, Switches e outros.
    
### Descrição da infraestrutura de cada ambiente:

- API REST:
    - Python 3.8
    - Aplicação escrita em Django 3.0
    - Django Rest Framework para a implementação da camada API Rest 


- Load Balance:
    - Nginx
    - Instância para balanceamento de cargas HTTP ( API )
    - Instância para balanceamento de cargas TCP ( Cluster Postgres )

    
- Banco de Dados:
    - Postgres
    - Cluster com 3 Nós
    - Cluster Control Orquestrador

    
- Filas:
    - RabbitMQ 
    - Fila para enfileiramento das requisições
    - Fila para enfileiramento das respostas às requisições

    
#### Infraestrutura Mínima (MVP):
Para esta entrega foi implementada a seguinte infraestrutura:

- Ambientes:
    - Ambiente DEV


- API REST:
    - Python 3.8
    - Aplicação escrita em Django 3.0
    - Django Rest Framework para a implementação da camada API Rest 


- Proxy Reverso:
    - Nginx
    - Instância para Proxy das requisições HTTP

    
- Banco de Dados:
    - PostgreSql
    - Modo de execução: Standalone

#### Justificativa pela escolha das tecnologias:

##### Django:

O Django é um dos mais conhecidos framewoks web para Python. Ele é robusto e estável 
sendo equivalente ou superior a frameworks de outras linguagens como: JSF/Hibernate (Java), 
Spring Framework (Java), Laravel (PHP) e etc. O DJango é constantemente atualizado, o que 
significa que novas features estão constantemente sendo adicionadas. Possui uma ótmia 
documentação e uma enorme comunidade ao redor do mundo. Esta escolha se deve a facilidade 
e a rapidez com o que é possível desenvolver utilizando-o.

##### Django Rest Framework:

Diferentemente do Django, o Django Rest Framework é uma camada acima do Django que habilita
a sua aplicação de forma rápida, fácil e segura de export *endpoints* da sua API REST.
Esta escolha se deu pelo deste framework ser totalmente compatível com Django e pela 
rapidez e facilidade no processo de desenvolvimento de uma API.

##### PostgreSQL:

É o banco de dados grátis mais eficiente do mercado. Possui diversos recursos dentre os 
quais podemos destacar: Suporte a tamanho ilimitado de linhas, consultas FullText, 
tabelas com até 16TB, aceita vários tipos de sub-consultas, possui um mecaisco eficiente
de segurança contra falhas, e etc. Também é possível executar o postgres em modo Cluster
aumentando ainda mais a resiliencia do ambiente.

##### NGINX:
O servidor web Nginx é um sistema completo. Ele está entre os top servidores de código da 
internet. Dentre as funcionalidades e benefícios podemos citar:

- Simples de implementar e proporcionar ao usuário segurança de ponta contra 
ataques a servidores web como DDoS e DoS.

- Ajuda a criar uma carga equilibrada ( Load Balance ) entre vários servidores back-end e proporciona 
cache para um servidor back-end mais lento.

- Não exige a configuração de um novo processo para cada nova solicitação da web vinda 
do cliente. Ao invés disso, a configuração padrão é para incluir apenas um processo 
de trabalho por CPU.

- Ele pode operar mais de 10000 conexões com uma pequena pegada de memória. 

- Pode operar múltiplos servidores da internet através de um único endereço de IP e 
entregar cada solicitação para o servidor dentro de uma LAN.

- Melhorar o desempenho de conteúdo estático. Adicionalmente, pode ser útil servir 
conteúdo em cache e executar criptografia SSL para diminuir a carga do servidor.

- Melhorar o tempo de carregamento.


#### Executar o projeto
Para rodar este projeto, você irá precisar ter o docker e o docker-compose instalados
no seu computador. Caso ainda não possua nenhum destes softwares, por favor, clique nos
links abaixo e realize a instalação:

Instalar Docker:

    https://docs.docker.com/engine/install/

Instalar Docker-Compose

    https://docs.docker.com/compose/install/

#### Execução do Projeto:

Realizar o clone do projeto deste repositório

Entrar dentro do diretório principal, onde se localiza o arquivo docker-compose.yml e 
executar o seguinte comando:

    docker-compose up -d --remove-orphans
    
Caso deseje para o ambiente, execute o seguinte comando:

    docker-compose down --remove-orphans
    
Após a execução deste comando o docker-compose irá realizar o download das imagens do projeto
e em seguida irá colocar a aplicação no ar. Assim que o sistema iniciar, uma das instancias irá
realizar o input de dados do dataset Wine. Esse processo vai levar alguns minutos mas você poderá
acessar a aplicação normalmente. Para acessar a aplicação acesse a seguinte URL em qualquer navegador:

    http://0.0.0.0/api/wine
    
Acessando esta URL você verá os 10 primeiros registros do banco de dados em uma lista
resumida, ou seja, somente com os campos id, country, designation e winery.

Para verificar as informações completas sobre um registro, escolha um registro da lista e
adicione o número ao final da URL da seguinte forma:

    http://0.0.0.0/api/wine/1
    
Este endpoint irá realizar uma busca no banco de dados e trazer todos os dados referentes ao 
registro com id 1.

#### Escalando a API REST

Na infraestrutura apresentada é possível escalar a API REST ou seja é possível executar
mais de uma réplica da aplicação REST API ao mesmo tempo. Para isso execute o comando `UP`
da seguinte forma:

    docker-compose up -d --scale application=3

#### Endpoints da API

##### Ações Básicas

Listagem de registros:

    http://0.0.0.0/api/wine
    
Detalhes de um registro:

    http://0.0.0.0/api/wine/<ID-REGISTRO>

##### Paginação
Para paginar os resultados das consultas utilize os parametros limit e offset.

Listar os 20 primeiros registros:

    http://0.0.0.0/api/wine?limit=20 

Listar os 20 próximos registros após os 20 primeiros:

    http://0.0.0.0/api/wine?limit=20&offset=20

Observação: 

- Por padrão, caso não seja passado nenhum dos parâmetros Offset e Limit a aplicação
configura os valores `limit=10` e `offset=0`, ou seja, os 10 primeiros registros no banco de dados

- O limite para retorno de registros em uma única requisição é de 100 registros, ou seja, caso o 
parâmetro limit seja maior que 100, ele só irá retornar os 100 primeiros registros que coincidam com 
a busca realizada.

##### Ordenação

Para ordernar os resultados por um campo específico utilize o parâmetro `ordering`:

    http://0.0.0.0/api/wine?ordering=country

Os campos disponíveis para ordenação são:
- country 
- variety
- points 
- price 
- winery

##### Filtragem

Para filtrar os resultados por um ou mais campos específicos utilize a seguinte sintaxe:

    http://0.0.0.0/api/wine?country=US&winery=Mirassou
    
Os campos disponíveis para filtrage são:
- country
- variety
- points 
- price 
- winery

##### Busca

É possível realizar uma consulta baseada em uma palavra-chave ( ou termo )
no campo `description`. Para realizar a busca utilize a seguint sintaxe:

    http://0.0.0.0/api/wine?description=Color
    
    http://0.0.0.0/api/wine?description=Caramelized
    
    http://0.0.0.0/api/wine?description=Aromatically
    
##### Misturando tudo

É possível realizar uma busca ativando todos as funcionalidades de busca.
Para tal, é necessário apenas inserir os parâmetros de acordo com as suas regras de utilização:

    http://0.0.0.0/api/wine?description=Wood&country=US&ordering=winery&limit=10&offset=100
    
    http://0.0.0.0/api/wine?country=US&ordering=winery&limit=10
        
    http://0.0.0.0/api/wine?ordering=winery
            
    http://0.0.0.0/api/wine?country=Spain&limit=5&offset=1&ordering=winery
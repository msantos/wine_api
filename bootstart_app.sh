[[ "$INPUT_DATA" == "False" ]] && {

  # Migração
  python manage.py makemigrations
  python manage.py migrate

  # Migracao do banco de dados
  python manage.py flush --no-input
  python manage.py collectstatic --no-input --clear

  # Iniciar Aplicacao
  gunicorn --workers=2 wine_api.wsgi:application --bind 0.0.0.0:8000 --log-file -
} || {

  # Input de dados ativo por 5 minutos
  timeout 300 gunicorn wine_api.wsgi:application

}